IoT Enabled Smart Traffic Systems

Link to Gitlab repository: https://gitlab.com/trystandeck/syse6301

	The purpose of this assignment was to model a real-world traffic situation in which two vehicles (a delivery vehicle and a traffic vehicle) follow a simple network. This was done using Gitlab, SUMO, Python, TraCI, and Iotery on a terminal on the MacOS Catalina operating system.
	Once everything was downloaded and the files were fetched, the coding portion of this project could begin. The original file contained a red vehicle that went along the road, looped on the east section of the map, and then disappeared to show that the vehicle had reached its final destination. The first task was to create another vehicle (blue vehicle) that followed the exact same route as the red. This was done by duplicating the code in simple.route.xml for the vehicle by changing the ID and color preferences as shown below. The same route code for the red vehicle was used for the blue vehicle in this step. 

<vehicle depart="1" id="veh0" route="route0" type="truck" departPos="free" color="red"/>
<vehicle depart="1" id="veh1" route="route0" type="truck" departPos="free" color="blue"/>

	The next task was to extend the blue vehicles route so that it returned to the initial starting position. This meant the blue vehicle had to take the same route back to the beginning in the opposite direction after it looped around the east loop. I created this path by duplicating the route id code and following the path back.

<route id="route1" edges="x1_s_nb main_3_eb main_4_eb main_5_eb main_6_eb eastloop_s_eb eastloop_e_nb eastloop_n_wb eastloop_w_sb main_6_wb main_5_wb main_4_wb main_3_wb x1_s_sb" />

	As can be seen from this, the path back included all the main streets, but the vehicle headed west instead of east which caused the edge name to end with wb. In the vehicle depart information, the blue vehicle’s route was changed to route1 to incorporate this change. The blue vehicle remained on this path without disappearing until it reached the end of x1_s_sb.
	Section 3 of this assignment involved setting up a virtual environment and installing dependencies. One big issue I faced in this assignment involved the incorrect placement of my SUMO_HOME files. When downloaded on to my mac, sumo made folder 1.6.0 instead of the README.md file’s 1.2.0. This small change was hard for me to find and set me back a lot of time trying to figure out. After this, python controller.py was ran in SUMO. The two coding tasks in section 3 involved calculating the fuel economy in MPG and making the blue vehicle stop for 30 seconds on the edge of main_3_wb. These two objectives were done using the code below.

traci.simulationStep()
fuel_consumption = traci.vehicle.getFuelConsumption("veh1")
current_road = traci.vehicle.getRoadID("veh1")

if traci.vehicle.getRoadID("veh1") == "x1_s_sb":
     dist = traci.vehicle.getDistance("veh1")
     speed = traci.vehicle.getSpeed("veh1")
     fuel_consumption = traci.vehicle.getFuelConsumption("veh1")
     break
print(step)

if current_road == "main_4_wb":
        traci.vehicle.setStop("veh1","main_3_wb", duration=30)

	The command traci.vehicle.getFuelConsumption retrieved the consumption in mL/s. The command traci.vehicle.getDistance retrieved the distance traveled in m. By dividing the distance by the fuel consumption times the number of steps and multiplying by the conversion factor for the MPG, the fuel economy value was found to be 2.78 MPG. 
	The road at which the blue vehicle needed to stop at was main_3_wb. The if current_road == “main_4_wb command signaled to the vehicle that it was going to run this command once this road was approached. Traci.vehicle.setStop caused the vehicle to stop at the very beginning of edge main_3_wb for a duration of 30 seconds.
	Section 4 involved executing the same code as section 3 but through Iotery. The Iotery dashboard was set up with the device types as asked in the program documents. The information for each vehicle is below. This information was put into iot.controller.py, and the file was implemented into SUMO.

	  
The deviceTypeUuid for the delivery vehicle was bea8baf3-f5f0-11ea-9df0-d283610663ec.
	Route Edge and Vehicle Speed were put in as data types in Iotery. The provided code in iot_controller.py was updated to show the speed and current road each of the two vehicles were on. 
speed = traci.vehicle.getSpeed("veh1")  # m/s
    current_road = traci.vehicle.getRoadID("veh1")  # current edge name
    speed_veh0 = traci.vehicle.getSpeed("veh0")  # m/s
    current_road_veh0 = traci.vehicle.getRoadID("veh0")  # current edge name

	The starting position of the traffic vehicle along with the entirety of the route was changed so that the red vehicle started at x2_s_nb and would go around the west loop instead of the east loop, ending at x1_n_nb. This was done similarly to that of section 1, and the code appeared as shown.

<route id="route0" edges="x2_s_nb main_4_wb main_3_wb main_2_wb main_1_wb westloop_n_wb westloop_w_sb westloop_s_eb westloop_e_nb main_1_eb main_2_eb x1_n_nb" />

	The more that was added to the simulation in Iotery, the slower the simulation and communication occured. Earlier, without Iotery, the simulation was 240 seconds. With Iotery, the counter in SUMO only went up to 142 seconds, yet it took an exceedingly longer amount of time to run fully through. Data would increase if the cars were going the same way the whole time or if only certain data analyses were done on the vehicles needed. No matter what, the more information included in the simulation, the slower the information was retrieved. It would be very important to prioritize what data was really needed in Iotery so that way you wouldn’t waste additional time and could speed up the simulation process.  
	Should you need to make the vehicles complete their routes faster, I would propose finding ways to avoid stops on the routes. The pauses at “red lights” or “traffic stops” seem to take a really long time in the Iotery simulation. Although this simulates real traffic components, it definitely held the vehicle’s time back. It would be useful to try to avoid traffic signals in the simulation or use tollways/ major highways at times when traffic was not cluttered so as to avoid delays.
	In step 5, the only difference from step 4 came from the addition of a Command Type in Iotery which was named Stop Vehicle on Main 6 E and did what the name said. The variable iotery_response_delivery_vehicles is the response from Iotery that contains commands. It appears as the following in the provided code.

    iotery_response_delivery_vehicle = delivery_vehicle_cloud_connector.postData(
        deviceUuid=delivery_vehicle["uuid"], data=data)

	For this command to work, the execute button for the demand must be pressed while the file is running. As the name says, when the blue delivery vehicle got to main_6_eb, it was stopped. However, unlike with the Traci.setStop command, the vehicle stopped in the middle of edge main_6_eb.
	If the Iotery dashboard was not usable, all of the commands could be automated through TraCI in SUMO. This was part of section 3 in this homework. For example, if you wanted to delay the vehicle for 30 seconds, this could be done through traci.vehicle.setStop. Additional traffic would come in as additional vehicles, such as how in section one, the blue vehicle was added to the simulation. <vehicle depart="1" id="veh1" route="route1" type="truck" departPos="free" color="blue"/>. 
	In the sumo simulation, you would input traffic lights as delays at roads with TraCI. In Iotery, a command would be set up in the system and you would then execute that command for the desired vehicle.
	This assignment took me many days to finish. Some of the code was outdated or had issues on my MacOS platform causing me to have to find solutions or ways around issues. I now feel like I could extend this platform to include additional components such as additional vehicles or more traffic signals. I had never used python or sumo before so this gave me a better understanding of the coding material to a point that I feel like I could explain and teach this same concept to others if needed.

Please see the following should you feel the questions throughout the assignment were not answered in enough detail.


1.	Setting up your environment
Question: What operating system are your running (Windows, MacOS, or Linux)?

I am running this on MacOS.

2.	Run your SUMO Network
Question:  Why do the vehicles disappear during the simulation?  At what simulation step do they disappear?  What edge of the network do they disappear on?

The vehicles disappear when they get to their final destination as is put in the code. They disappear at the end of the edge known as eastloop_w_sb

Question: At approximately what simulation step does your blue vehicle with the extended route leave the network?

The blue vehicle leaves the network where it started (at the end of the edge with name x1_s_sb).

3.	Controlling SUMO from TraCI
***Question: What is the fuel economy in MPG of the vehicle you added over its route?

I found the MPG of the blue vehicle (veh1) by finding the distance(m), fuel consumption(mL/s) , and step count over the entire route and converting these values appropriately to get to MPG. The MPG was calculated to be 2.78 MPG.

Question: At approximately what simulation step does your vehicle stop for 30 seconds?  Where along the edge does it stop? 

The simulation stops at the very beginning of edge main_3_wb for 30 seconds as specified as the duration during this period.

4.	Connecting your Vehicle via IoT (V2I) with Iotery
Question: What are the uuids for the delivery_vehicle and traffic_vehicle?

Delivery_vehicle uuid:  fd9bdffe-f5f0-11ea-9df0-d283610663ec

Traffic_vehicle uuid: d81e3b23-f7a4-11ea-9df0-d283610663ec


Question: What is the deviceTypeUuid of your delivery vehicle?

deviceTypeUuid: bea8baf3-f5f0-11ea-9df0-d283610663ec

Question: Does the communications slow down when you added the other vehicle?  What are some ways you can think of that would help increase data throughout if given more data to push to the cloud?

The more that was added to the simulation in Iotery, the slower the simulation and communication occurs. Earlier, without Iotery, the simulation was 242 seconds. With Iotery, the counter in SUMO only went up to 142 seconds, yet it took an exceedingly longer amount of time to run fully through. Data would increase if the cars were going the same way the whole time or if only certain data analyses were done on the vehicles needed. No matter what, the more information included in the simulation, the slower the information is retrieved. It would be very important to prioritize what data was really needed in Iotery so that way you wouldn’t waste additional time and could speed up the simulation process.  

Question: If we wanted to make the vehicles complete their routes faster, what you would propose doing?

I would propose finding ways to avoid stops on the routes. The pauses at “red lights” or traffic stops seem to take a really long time in the Iotery simulation. Although this simulates real traffic components, it definitely held the vehicles time back. It would be useful to try to avoid traffic signals in the simulation of a real road or use tollways/ major highways at times when traffic was not cluttered. 

5.	Commanding your Vehicle via Iotery
Question: How is the response from Iotery structured?

The command, once put into Iotery, stopped the blue car in the middle of edge main_6_eb. The command must be started for the Delivery Vehicle by going to the screen and clicking post command. The data appears as below.

STOP VEHICLE ON MAIN 6 E
Set command instance executed
{u'status': u'success', u'unexecutedCommands': {u'device': [{u'setExecutedTimestamp': None, u'commandTypeEnum': u'STOP_VEHICLE_MAIN_6_E', u'batchReferenceUuid': None, u'uuid': u'82cfe5fb-f9c2-11ea-9df0-d283610663ec', u'isUnexecuted': True, u'commandFields': {}, u'timestamp': 1600442440, u'created': u'2020-09-18T15:20:40.000Z', u'commandTypeUuid': u'9e7160b6-f60d-11ea-9df0-d283610663ec', u'deviceUuid': u'fd9bdffe-f5f0-11ea-9df0-d283610663ec', u'_data': [], u'name': None}], u'network': []}, u'activeNotificationInstances': {u'device': [], u'network': []}}

Question: What happened when the vehicle reached the stopping point?

The car stopped in the middle of the edge main_6_eb, and the terminal printed Stop Vehicle on Main 6 E.

6.	Concepts
Question: If you didn’t have the Iotery Dashboard (or a human-in-the-loop), how would you go about automating the commands?

The commands can be put into sumo through TraCI. This was displayed in section 3. For example, if you wanted to delay the vehicle for 30 seconds, this could be done through traci.vehicle.setStop. Otherwise, Iotery competitors include Enhancv, Reportei, and FreshLime, and all of these could be used as alternatives.

Question: How would you introduce additional traffic onto the simulation?

Additional traffic would come in as additional vehicles, such as how in section one, the blue vehicle was added to the simulation.
<vehicle depart="1" id="veh1" route="route1" type="truck" departPos="free" color="blue"/>

Question: What commands would you use to control traffic lights in the SUMO simulation?  How would you structure Iotery to help you do this?

In the sumo simulation, you would input traffic lights as delays at roads with TraCI. 
In Iotery, a command would be set up in the system and you would then execute that command for the desired vehicle.

Question: How long did it take you to complete this assignment?  Do you feel you understand and could extend the platform you completed to include additional components?  

This assignment took me many days to finish. Some of the code was outdated or had issues on my MacOS platform causing me to have to find solutions or ways around. I now feel like I could extend this platform to include additional components such as additional vehicles or more traffic signals. I had never used python or sumo before so this gave me a better understanding of the coding material to a point that I feel I could explain and teach this same concept to others if needed.


